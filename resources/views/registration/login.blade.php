@extends('registration.master')

@section('title')
Login
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
<div class="row">
	<div class="col-7 text-right">Login </div>
<div class="col-5 text-right">{!!Html::link("/register",'Register',['class' => 'btn btn-primary'])!!}
</div>
</hr>
@endsection

{!!Form::open(array('action'=>'RegistrationController@loginin'))!!}
	@csrf
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('name','Name:')!!}
		</div>
		<div class="col-7">
			{!!Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder'=>'name', 'required'])!!}
			{!!$errors->first('name')!!}
		</div>
	</div><br/>
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('email','E-mail:')!!}
		</div>
		<div class="col-7">
			{!!Form::email('email', old('email'), ['class' => 'form-control', 'id' => 'email','placeholder'=>'unique@email','required'])!!}
			{!!$errors->first('email')!!}
		</div>
	</div><br/>
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('password','Password:')!!}
		</div>
		<div class="col-7">
			{!!Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder'=>'password', 'required'])!!}
			{!!$errors->first('password')!!}
		</div>
	</div><br/>
	<div class="form-group row">
				<div class="col-5">
					{!!Form::label('type','Type:')!!}
				</div>
				<div class="col-7">
					{{ Form::select('type', ['Visitor' => 'Visitor', 'Registered' => 'Registered'], 'Visitor', ['class' => 'form-control']) }}
					{!!$errors->first('type')!!}
				</div>
			</div><br/>
	<div class="form-group row">
		<div class="col-12 d-flex justify-content-center">
			{{Form::hidden('id')}}
			{!!Form::submit('Login', ['class' => 'btn btn-primary'])!!}
		</div>
	</div>
	@include('errors')
{!!Form::close()!!}

@if(Session::has('alert'))
    {{Session::get('alert')}}
@endif

@endsection
