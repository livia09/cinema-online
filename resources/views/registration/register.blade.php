@extends('registration.master')

@section('title')
Register
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
	Register form
@endsection


{!!Form::open(array('action'=>'RegistrationController@store'))!!}
	@csrf
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('name','Name:')!!}
		</div>
		<div class="col-7">
			{!!Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder'=>'name', 'required'])!!}
			{!!$errors->first('name')!!}
		</div>
	</div><br/>
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('age','Age:')!!}
		</div>
		<div class="col-7">
			{!!Form::text('age', old('age'), ['class' => 'form-control', 'id' => 'age', 'placeholder'=>'years', 'required'])!!}
			{!!$errors->first('age')!!}
		</div>
	</div><br/>
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('email','E-mail:')!!}
		</div>
		<div class="col-7">
			{!!Form::email('email', null, ['class' => 'form-control', 'id' => 'email','placeholder'=>'new_unique@email','required'])!!}
			{!!$errors->first('email')!!}
		</div>
	</div><br/>
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('password','Password:')!!}
		</div>
		<div class="col-7">
			{!!Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder'=>'password', 'required'])!!}
			{!!$errors->first('password')!!}
		</div>
	</div><br/>
	<div class="form-group row">
		<div class="col-5">
			{!!Form::label('confirm_password','Confirm password:')!!}
		</div>
		<div class="col-7">
			{!!Form::password('confirm_password', ['class' => 'form-control', 'id' => 'confirm_password', 'placeholder'=>'confirm password', 'required'])!!}
			{!!$errors->first('confirm_password')!!}
		</div>
	</div><br/>
	<div class="form-group row">
				<div class="col-5">
					{!!Form::label('type','Type:')!!}
				</div>
				<div class="col-7">
					{{ Form::select('type', ['Visitor' => 'Visitor', 'Registered' => 'Registered'], 'Visitor', ['class' => 'form-control']) }}
					{!!$errors->first('type')!!}
				</div>
			</div><br/>
	<div class="form-group row">
		<div class="col-12 d-flex justify-content-center">
			{{Form::hidden('id')}}
			{!!Form::submit('Register now', ['class' => 'btn btn-primary'])!!}
		</div>
	</div>
	@include('errors')
{!!Form::close()!!}

@endsection