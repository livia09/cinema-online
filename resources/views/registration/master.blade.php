<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/cinema-online/css/style.css">
	<title>@yield('title')</title>
</head>
<body>

<div class = "container bg-dark p-6 my-2 border border-primary rounded-sm">

	<div class = "container bg-info p-3 border border-primary rounded-sm">
    <div class="d-flex justify-content-center display-4 text-white">
      @yield('header')
    </div>
  </div> 

	<div class="d-flex justify-content-center">	
		<div class="card bg-light p-5 m-5" style="width:500px;">
			<div class="card-header">		
				<h2 class="text-center text-primary">
					@yield('content_title')					
				</h2>
			</div>

			<div clas="card-body">
				<br>
				@yield('content')
			</div>		
		</div>
	</div>
</div>
</body>
</html>