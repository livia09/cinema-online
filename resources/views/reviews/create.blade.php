@extends('layout')

@section('title')
Reviews
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Add a review
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link active" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 p-5">Add review</h1>
		</div>
		<div class="card-body">
			{!!Form::open(array('action'=>'Admin\ReviewsController@store'))!!}
			@csrf
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('movie_id','Movie id:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('movie_id', null, ['class' => 'form-control', 'id' => 'movie_id',  'placeholder' =>'movie id', 'required'])!!}
					{!!$errors->first('movie_id')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('title','Title:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('title', null, ['class' => 'form-control', 'id' => 'title',  'placeholder' =>'title review', 'required'])!!}
					{!!$errors->first('title')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('member','Member:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('member', null, ['class' => 'form-control', 'id' => 'member',  'placeholder' =>'name', 'required'])!!}
					{!!$errors->first('member')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('rating','Rating:')!!}
				</div>				
				<div class="col-8">
					{!!Form::number('rating', 5,['class' => 'form-control', 'id' => 'rating', 'placeholder' =>'from 0 to 10', 'required', 'min'=>1, 'max'=>10])!!}
					{!!$errors->first('rating')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('description','Description:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'placeholder' =>'opinion about movie', 'required'])!!}
					{!!$errors->first('description')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{{Form::hidden('id')}}
					{!!Form::submit('Add review', ['class' => 'btn btn-primary'])!!}
				</div>
				@include('errors')
				{!!Form::close()!!}
			</div>
		</div>
	</div>
</div>
@endsection