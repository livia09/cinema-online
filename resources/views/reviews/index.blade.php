@extends('layout')

@section('title')
Reviews
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Reviews
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link active" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

@if(count($reviews)==0)
<div class="d-flex justify-content-center">No reviews in database!</div>
@else
<table class="container table table-striped table-hover border border-info  text-center py-4 mt-5">
  <tr class="bg bg-primary text-white">
    <th>Member</th>
    <th>Movie id</th>
    <th>Rating</th>
    <th>Title</th>
    <th>Description</th>
    <th>Actions</th>
  </tr>
  @foreach($reviews as $review)
  <tr>
    <td>{{$review->member}}</td>
    <td>{{$review->movie_id}}</td>
    <td>{{$review->rating}}</td>
    <td>{{$review->title}}</td>
    <td>
      @if(strlen($review->description) > 22)
        {{str_pad(substr($review->description,0,22),25,".") }}
      @else
        {{$review->description}}
      @endif
    </td>
    <td>
      <div class = "row">
        <div class="col-sm">
          {!!Html::link("admin/reviews/{$review->id}",'View',['class' => 'btn btn-primary m-1'])!!}
        </div>
        <div class="col-sm">
          {!!Html::link("admin/reviews/{$review->id}/edit",'Edit',['class' => 'btn btn-primary m-1'])!!}
        </div>
          <div class="col-sm">
            <form method="post" action="/cinema-online/admin/reviews/<?php echo $review->id;?>">
              <?php echo method_field('Delete');?>
              <?php echo csrf_field(); ?>
              <button type="submit" class ='btn btn-primary m-1'>Delete</button>
            </form>
          </div>
        </div>
      </td>
    </tr>
  @endforeach
</table>
@endif

<br/>
<div class="container d-flex justify-content-center">
  {!!Html::link("admin/reviews/create",'New review',['class' => 'btn btn-primary m-1'])!!}</div>
  <br/><br/>
  <div class="d-flex justify-content-center">
    @if(Session::has('message'))
    {{Session::get('message')}}
    @endif
  </div>
</div>
@endsection
