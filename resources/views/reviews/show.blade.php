@extends('layout')

@section('title')
Show review
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Details review
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link active" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="jumbotron">
	<h1 class="p-2 text-primary">Details for: {{$review->member}}</h1>
	<br>
	<h3 class="p-2">Id: {{$review->id}}</h3>
	<h3 class="p-2">Movie id: {{$review->movie_id}}</h3>
	<h3 class="p-2">Title: {{$review->title}}</h3>
	<h3 class="p-2">Rating: {{$review->rating}}</h3>
	<h3 class="p-2">Description: {{$review->description}}</h3>
	<br>
	<span class="p-2">
		{!!Html::link("admin/reviews",'Back', ['class' => 'btn btn-primary'])!!}
	</span>
</div>

@endsection