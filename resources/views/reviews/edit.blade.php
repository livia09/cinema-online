@extends('layout')

@section('title')
Reviews
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Edit a review
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link active" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-director">
			<h1 class="text-primary p-3 pl-5">Edit review</h1>
		</div>
		<div class="card-body">
			{!!Form::model($review, array('url'=>'/admin/reviews/'.$review->id))!!}
				{{Form::hidden('id')}} 
				@csrf				
				{{ method_field('PATCH') }}
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('movie_id','Movie id:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('movie_id', $review->movie_id, ['class' => 'form-control', 'id' => 'movie_id', 'required'])!!}
					{!!$errors->first('movie_id')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('member','Member:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('member', $review->member, ['class' => 'form-control', 'id' => 'member', 'required'])!!}
					{!!$errors->first('member')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('rating','Rating:')!!}
				</div>				
				<div class="col-8">
					{!!Form::number('rating', $review->rating,['class' => 'form-control', 'id' => 'rating', 'required','min'=>1,'max'=>10])!!}
					{!!$errors->first('rating')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('title','Title:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('title',  $review->title, ['class' => 'form-control', 'id' => 'title', 'required', 'placeholder' =>''])!!}
					{!!$errors->first('title')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('description','Description:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('description', $review->description, ['class' => 'form-control', 'id' => 'description', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'required'])!!}
					{!!$errors->first('description')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{!!Form::submit('Update review', ['class' => 'btn btn-primary'])!!}
				</div>
			</div>
			@include('errors')
			{!!Form::close()!!}
		</div>
	</div>
</div>

@endsection