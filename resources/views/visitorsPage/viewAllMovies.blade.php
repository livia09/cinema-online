@extends('visitorsPage.view')

@section('title')
Movies
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Movies page
@endsection

<br>
<div class="d-flex justify-content-center"> 
  {{ $allmovies->onEachSide(1)->links() }} 
</div>

<div class='row d-flex justify-content-center'>
@foreach($allmovies as $movie)
  @if(Session::get('age')<$movie->age_restrict)       
    <h5 class="text-white"><br>{!! $string = "Restricted movie! <br>" !!} </h5>    
  @else
  <div class="col-5 text-white">
    <h3 class="text-left text-primary">Details about movie</h3><br>
    <div class='row mb-1'>
      <div class='col-5'>Title:</div>
      <div class='col-7'>{{$movie->title}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Release date:</div>
      <div class='col-7'>{{$movie->release_date}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Runnimg time:</div>
      <div class='col-7'>{{$movie->running_time}} min</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Genre:</div>
      <div class='col-7'>{{$movie->film_genre}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Cast:</div>
      <div class='col-7'>{{$movie->cast}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Director:</div>
      <div class='col-7'>{{$movie->director}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Production:</div>
      <div class='col-7'>{{$movie->production}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Original language:</div>
      <div class='col-7'>{{$movie->language}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Age restrction:</div>
      <div class='col-7'>{{$movie->age_restrict}}</div>
    </div>
  </div>    
  <div class="col-7">
    <div class="embed-responsive embed-responsive-4by3">
      {!! htmlspecialchars_decode($movie->iframe, ENT_NOQUOTES) !!} 
    </div>
  </div>
  @endif
<br>
<div class="col-12 text-white">
  <h3 class="text-left text-primary">Reviews</h3>
  @foreach($allReviews[$movie->id] as $review)
  <div class='row mb-1'>
    <div class='col-5'>Title:</div>
    <div class='col-7'>{{ $review->title }}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Rating:</div>
    <div class='col-7'>{{ $review->rating }}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Description:</div>
    <div class='col-7'>{{ $review->description }}</div>
  </div>
  <hr> 
  @endforeach
  </div>
@endforeach
</div>

@endsection