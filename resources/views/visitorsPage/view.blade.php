<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/cinema-online/css/style.css">
  <title>@yield('title')</title>
</head>
<body>
@if(!empty(Session()) && Session::has('type'))
  <div class = "container bg-dark p-4 my-2 border border-primary rounded-sm">

  <div class = "container bg-info p-3 my-2 border border-primary rounded-sm">
    <div class="d-flex justify-content-center display-4 text-white">
      @yield('header')
    </div>
  </div>

  <h2 class="text-center text-white">
    @yield('content_title')
  </h2>
  
  <div class="row bg-info border border-primary rounded-sm p-1 m-0">
    <div class="col">
      <nav class="nav">
        <a class="nav-link text-light" href="/cinema-online/visitor">First page</a>
        @if(Session::has('type') && (Session::get('type') == 'Visitor' || Session::get('type') == 'Admin'))
        <a class="nav-link text-light" href="/cinema-online/visitor/movies">Movies</a>
         @endif
         @if(Session::has('type') && (Session::get('type') == 'Registered' || Session::get('type') == 'Admin'))
         <a class="nav-link text-light" href="/cinema-online/visitor/movies-theaters">Book a movie theater</a> 
        @endif
      </nav>
    </div>
    <div class="col">
      <nav class="nav d-flex justify-content-end">
        <a class="nav-link text-light" href="/cinema-online/login">Login</a>      
        <a class="nav-link text-light" href="/cinema-online/logout">Logout</a>
      </nav>
    </div>
  </div>

  @yield('content')

</div>
@else
<br><br>
<h3 class="text-center">{{$message="Session for this page is logout!"}}</h3>
@endif
</body>
</html>
