@extends('visitorsPage.view')

@section('title')
Movie theaters
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Movies theaters page
@endsection

@if(Session::has('type') && (Session::get('type') == 'Registered'|| Session::get('type') == 'Admin'))

<br>
<div class="text-center text-white">
  @if(Session::has('message'))
    {{Session::get('message')}}
  @endif
</div>
<br>
<div class='row'>
  @foreach($allTheaters as $movieth)
  <div class="col-6">
   <div class="jumbotron p-4 m-3 border border-primary bg-info text-white" style="min-height: 250px">
    <div class='row my-2'>
      <div class='col-4'>Movie theater:</div>
      <div class='col-8'>{{$movieth->theater_name}}</div>
    </div>
    <div class='row my-2'>
     <div class='col-4'>Movies:</div>
     <div class='col-8'>
        @foreach($allMovies[$movieth->id] as $movie)
          {{ $movie->title }}<br>
        @endforeach
    </div>
  </div>
  <div class='col-12 text-center my-2'>
     {!!Html::link("visitor/movies-theaters/{$movieth->id}",'Book a movie',['class' => 'btn btn-success m-1'])!!}
  </div>
</div>
</div>
@endforeach
</div>
@else
<br><br>
<h5 class="text-center text-white">{{$message="Session for this page is logout!"}}</h5>
@endif

@endsection