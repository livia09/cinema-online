@extends('visitorsPage.view')

@section('title')
Movie theaters
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
{{ $movie->title }} 
@endsection

<div class="d-flex justify-content-center p-5">
	<div class="card bg-dark border border-primary" style="width: 30rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 p-5">Add review</h1>
		</div>
		<div class="card-body text-white">
			{!!Form::open(array('action'=>'MoviesController@store'))!!}

			@csrf

			@if (isset($movie->id))
				{!! Form::hidden('movie_id', $movie->id) !!}
			@endif
			
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('title','Title:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('title', null, ['class' => 'form-control', 'id' => 'title',  'placeholder' =>'title review', 'required'])!!}
					{!!$errors->first('title')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('member','Member:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('member', null, ['class' => 'form-control', 'id' => 'member',  'placeholder' =>'name', 'required'])!!}
					{!!$errors->first('member')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('rating','Rating:')!!}
				</div>				
				<div class="col-8">
					{!!Form::number('rating', 5,['class' => 'form-control', 'id' => 'rating', 'placeholder' =>'from 0 to 10', 'required', 'min'=>1, 'max'=>10])!!}
					{!!$errors->first('rating')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('description','Description:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'placeholder' =>'opinion about movie', 'required'])!!}
					{!!$errors->first('description')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{{Form::hidden('id')}}
					{!!Form::submit('Add review', ['class' => 'btn btn-primary'])!!}
				</div>
				@include('errors')
				{!!Form::close()!!}
			</div>
		</div>
	</div>
</div>

@endsection