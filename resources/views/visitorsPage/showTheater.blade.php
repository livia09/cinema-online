
@extends('visitorsPage.view')

@section('title')
Movie theaters
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
{{ $movietheater->theater_name }} - Movies
@endsection


@foreach($allMovies[$movietheater->id] as $key => $movie)
<div class='row d-flex justify-content-center border border-primary rounded-sm p-4 m-4'>
  @if(Session::get('age') < $movie->age_restrict)       
  <h5 class="text-white"><br>{!! $string = "Item is not posted! <br>" !!} </h5>    
  @continue     
  @else
  <div class=" col-5 text-white">
    <h3 class="text-left text-primary">Details about movie</h3><br>
    <div class='row mb-1'>
      <div class='col-5'>Title:</div>
      <div class='col-7'>{{$movie->title}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Release date:</div>
      <div class='col-7'>{{$movie->release_date}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Runnimg time:</div>
      <div class='col-7'>{{$movie->running_time}} min</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Genre:</div>
      <div class='col-7'>{{$movie->film_genre}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Cast:</div>
      <div class='col-7'>{{$movie->cast}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Director:</div>
      <div class='col-7'>{{$movie->director}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Production:</div>
      <div class='col-7'>{{$movie->production}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Original language:</div>
      <div class='col-7'>{{$movie->language}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Age restrction:</div>
      <div class='col-7'>{{$movie->age_restrict}}</div>
    </div>
    <hr>

    <div class='row mb-1'>
    	<div class="col-4">Date</div>
    	<div class="col-4">Time</div>
      <?php if(Session::get('age')<=10)
      echo $a="<div class='col-4'>Half price (cents)</div>";
      else
        echo $a="<div class='col-4'>Price (cents)</div>";
      ?>
      <div class="col-1"></div>

    </div>
    
    {!!Form::open(array('action'=>'MoviesController@bookingstore'))!!}
    @csrf
    {{Form::hidden('theater_id', $movietheater->id)}}
    {{Form::hidden('movie_id', $movie->id)}}
    {{Form::hidden('theater_name', $movietheater->theater_name)}}
    {{Form::hidden('movie_title', $movie->title)}}

    <div class='row'>
      <div class="col-4">
        {!!Form::text('date', $movie->release_date, ['class' => 'form-control', 'id' => 'hours', 'readonly', 'required'])!!}
      </div>
      <div class="col-4">
        {!!Form::text('time', $movietheater->theater_hours, ['class' => 'form-control', 'id' => 'hours', 'readonly', 'required'])!!}
      </div>
      <?php
      if(Session::get('age') <= 10)
        $price = (float)($movie->price/2);
      else
        $price =  $movie->price;
      ?>
      <div class="col-4">
        {!!Form::text('price', $price, ['class' => 'form-control', 'id' => 'hours', 'readonly', 'required'])!!}
      </div>
      <div class="col-1">
        {!!Form::submit('Book', ['class' => 'btn btn-primary'])!!}
      </div>
    </div>
    {{Form::hidden('member', Session::get('name'))}}
    {{Form::hidden('email', Session::get('email'))}}
    {!!Form::close()!!}
    <hr>
  </div>
  <div class="col-7">
    <div class="embed-responsive embed-responsive-4by3">
      {!! htmlspecialchars_decode($movie->iframe, ENT_NOQUOTES) !!} 
    </div>
  </div>

  <br><br>
  <div class="col-12 text-white">
    <div class='row mb-2'>
      <div class='col-5'><h3 class="text-left text-primary">Reviews</h3></div> 
      <div class='col-7'>{!!Html::link("/visitor/movies-theaters/{$movie->id}/add-review",'Give a review to movie',['class' => 'btn btn-primary my-1'])!!}</div>
    </div>
  </br>
  @if(!isset($allReviews[$movie->id]) && empty($allReviews[$movie->id]))
    <div class="col-12 text-white">
      Still no reviews, maybe you would like to give one!
    </div>
  @else
    @foreach($allReviews[$movie->id] as $review)
      <div class='row mb-1'>
        <div class='col-5'>Title:</div>
        <div class='col-7'>{{ $review->title }}</div>
      </div>
      <div class='row mb-1'>
        <div class='col-5'>Rating:</div>
        <div class='col-7'>{{ $review->rating }}</div>
      </div>
      <div class='row mb-1'>
        <div class='col-5'>Description:</div>
        <div class='col-7'>{{ $review->description }}</div>
      </div>
      <hr> 
    @endforeach
  @endif
</div>
@endif
</div>
@endforeach

@endsection

