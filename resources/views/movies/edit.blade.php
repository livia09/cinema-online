@extends('layout')

@section('title')
movies
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Edit a movie
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link active" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-director">
			<h1 class="text-primary p-3 pl-5">Edit movie</h1>
		</div>
		<div class="card-body">
			{!!Form::model($movie, array('url'=>'/admin/movies/'.$movie->id))!!}
				{{Form::hidden('id')}} 
				@csrf				
				{{ method_field('PATCH') }}
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('movieth_id','Theater movie id:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('movieth_id', $movie->movieth_id, ['class' => 'form-control', 'id' => 'movieth_id', 'required'])!!}
					{!!$errors->first('movieth_id')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('title','Title:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('title', $movie->title, ['class' => 'form-control', 'id' => 'title', 'required'])!!}
					{!!$errors->first('title')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('iframe','Trailer:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('iframe', $movie->iframe, ['class' => 'form-control', 'id' => 'iframe', 'rows' => 7, 'cols' => 54, 'style' => 'resize:none', 'required'])!!}
					{!!$errors->first('iframe')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('release_date','Release date:')!!}
				</div>				
				<div class="col-8">
					{!!Form::text('release_date', $movie->release_date, ['class' => 'form-control', 'id' => 'release_date', 'required'])!!}
					{!!$errors->first('release_date')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('running_time','Running time:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('running_time', $movie->running_time, ['class' => 'form-control', 'id' => 'running_time', 'required'])!!}
					{!!$errors->first('running_time')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('film_genre','Film genre:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('film_genre', $movie->film_genre, ['class' => 'form-control', 'id' => 'film_genre', 'required'])!!}
					{!!$errors->first('film_genre')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('cast','Cast:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('cast', $movie->cast, ['class' => 'form-control', 'id' => 'cast', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'required'])!!}
					{!!$errors->first('cast')!!}
				</div>
			</div><br/>			
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('director','Director:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('director', $movie->director, ['class' => 'form-control', 'id' => 'director', 'required'])!!}
					{!!$errors->first('director')!!}
				</div>
			</div><br/>
			
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('production','Production:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('production', $movie->production, ['class' => 'form-control', 'id' => 'production', 'required'])!!}
					{!!$errors->first('production')!!}
				</div>
			</div><br/>

			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('language','Language:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('language', $movie->language, ['class' => 'form-control', 'id' => 'language', 'required'])!!}
					{!!$errors->first('language')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('age_restrict','Age restriction:')!!}
				</div>
				<div class="col-8">
					{{ Form::select('age_restrict', ['0' => '0', '16' => '16'], $movie->age_restrict, ['class' => 'form-control']) }}
					{!!$errors->first('age_restrict')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('price','Price:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('price', $movie->price, ['class' => 'form-control', 'id' => 'price', 'required'])!!}
					{!!$errors->first('price')!!}
				</div>
			</div><br/>			
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{{Form::hidden('id')}}
					{!!Form::submit('Update movie', ['class' => 'btn btn-primary'])!!}
				</div>
			</div>
			@include('errors')
			{!!Form::close()!!}
		</div>
	</div>
</div>

@endsection