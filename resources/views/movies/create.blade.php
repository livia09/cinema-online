@extends('layout')

@section('title')
Movies
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Add a movie
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link active" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 p-5">Add a movie</h1>
		</div>
		<div class="card-body">
			{!!Form::open(array('action'=>'Admin\MoviesController@store'))!!}
			@csrf
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('movieth_id','Theater movie id:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('movieth_id', null, ['class' => 'form-control', 'id' => 'movieth_id', 'placeholder'=>'movie theater','required'])!!}
					{!!$errors->first('movieth_id')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('title','Title:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('title', null, ['class' => 'form-control', 'id' => 'title', 'placeholder'=>'movie title','required'])!!}
					{!!$errors->first('title')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('iframe','Trailer:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('iframe', null, ['class' => 'form-control', 'id' => 'iframe', 'rows' => 7, 'cols' => 54, 'style' => 'resize:none', 'placeholder' => 'embed code youtobe trailer', 'required'])!!}
					{!!$errors->first('iframe')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('release_date','Release date:')!!}
				</div>				
				<div class="col-8">
					{!!Form::text('release_date', null, ['class' => 'form-control', 'id' => 'release_date', 'placeholder'=>'yyyy-mm-dd','required'])!!}
					{!!$errors->first('yyyy-mm-dd')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('running_time','Running time:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('running_time', null, ['class' => 'form-control', 'id' => 'running_time', 'placeholder'=>'minutes', 'required'])!!}
					{!!$errors->first('running_time')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('film_genre','Film genre:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('film_genre', null,['class' => 'form-control', 'id' => 'film_genre', 'placeholder'=>'movie genre', 'required'])!!}
					{!!$errors->first('film_genre')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('cast','Cast:')!!}
				</div>
				<div class="col-8">
					{!!Form::textarea('cast', null,['class' => 'form-control', 'id' => 'cast', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'placeholder'=>'movie cast', 'required'])!!}
					{!!$errors->first('cast')!!}
				</div>
			</div><br/>			
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('director','Director:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('director', null, ['class' => 'form-control', 'id' => 'director', 'placeholder'=>'director name', 'required'])!!}
					{!!$errors->first('director')!!}
				</div>
			</div><br/>
			
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('production','Production:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('production', null, ['class' => 'form-control', 'id' => 'production', 'placeholder'=>'year', 'required'])!!}
					{!!$errors->first('production')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('language','Language:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('language', null, ['class' => 'form-control', 'id' => 'language', 'placeholder'=>'language', 'required'])!!}
					{!!$errors->first('language')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('age_restrict', 'Age restriction:')!!}
				</div>
				<div class="col-8">
					{{ Form::select('age_restrict', ['0' => '0', '16' => '16'], '0', ['class' => 'form-control']) }}
					{!!$errors->first('age_restrict')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('price','Price:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('price', null, ['class' => 'form-control', 'id' => 'price', 'placeholder'=>'0.0 euro', 'required'])!!}
					{!!$errors->first('price')!!}
				</div>
			</div><br/>		
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{{Form::hidden('id')}}
					{!!Form::submit('Add movie', ['class' => 'btn btn-primary'])!!}
				</div>
				@include('errors')
				{!!Form::close()!!}
			</div>
		</div>
	</div>
</div>
@endsection