@extends('layout')

@section('title')
Movies
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Movies
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link active" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

@if(count($movies)==0)
<div class="d-flex justify-content-center">No movies in database!</div>
@else
<table class="container table table-striped table-hover border border-info  text-center py-4 mt-5">
  <tr class="bg bg-primary text-white">
    <th>Title</th>
    <th>Release date</th>
    <th>Running time</th>
    <th>Film genre</th>
    <th>Cast</th>
    <th>Director</th>
    <th>Production</th>
    <th>Language</th>
    <th>Age restrict</th>
    <th>Price</th>
    <th>Actions</th>
  </tr>
  @foreach($movies as $movie)
  <tr>
    <td>{{$movie->title}}</td>
    <td>{{$movie->release_date}}</td>
    <td>{{$movie->running_time}} minutes</td>
    <td>{{$movie->film_genre}}</td>
    <td>{{$movie->cast}}</td>
    <td>{{$movie->director}}</td>
    <td>{{$movie->production}}</td>
    <td>{{$movie->language}}</td>
    <td>{{$movie->age_restrict}} years</td>
    <td>{{$movie->price}} cents</td>
  </div>
  <td>
    <div class = "row">
      <div class="col-sm">
        {!!Html::link("admin/movies/{$movie->id}",'View',['class' => 'btn btn-primary m-1'])!!}
      </div>
      <div class="col-sm">
        {!!Html::link("admin/movies/{$movie->id}/edit",'Edit',['class' => 'btn btn-primary m-1'])!!}
      </div>
        <div class="col-sm">
          <form method="post" action="/cinema-online/admin/movies/<?php echo $movie->id; ?>">
            <?php echo method_field('Delete'); ?>
            <?php echo csrf_field(); ?>
            <button type="submit" class ='btn btn-primary m-1'>Delete</button>
          </form>
        </div>
      </div>
    </td>
  </tr>
  @endforeach
</table>
@endif

<br/>
<div class="container d-flex justify-content-center">
  {!!Html::link("admin/movies/create",'New movie',['class' => 'btn btn-primary m-1'])!!}</div>
  <br/><br/>
  <div class="d-flex justify-content-center">
    @if(Session::has('message'))
      {{Session::get('message')}}
    @endif
  </div>
</div>
@endsection