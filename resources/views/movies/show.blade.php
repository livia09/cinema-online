@extends('layout')

@section('title')
Show movie
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Details movie
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link active" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="jumbotron">
	<h1 class="p-2 text-primary">Details for: {{$movie->title}}</h1>
	<br>
	<h3 class="p-2">Id: {{$movie->id}}</h3>
	<h3 class="p-2">Movie theater id: {{$movie->movieth_id}}</h3>
	<h3 class="p-2">Release date: {{$movie->release_date}}</h3>
	<h3 class="p-2">Running time: {{$movie->running_time}} minutes</h3>
	<h3 class="p-2">Film genre: {{$movie->film_genre}}</h3>
	<h3 class="p-2">Cast: {{$movie->cast}}</h3>
	<h3 class="p-2">Director: {{$movie->director}}</h3>
	<h3 class="p-2">Production: {{$movie->production}}</h3>
	<h3 class="p-2">Language: {{$movie->language}}</h3>
	<h3 class="p-2">Age restriction: {{$movie->age_restrict}}</h3>
	<h3 class="p-2">Price: {{$movie->price}} cents</h3>
	<h3 class="p-2">Trailer script: {{$movie->iframe}}</h3>
	<br>
	<span class="p-2 d-flex justify-content-center">
		{!!Html::link("admin/movies",'Back', ['class' => 'btn btn-primary'])!!}
	</span>
</div>
@endsection