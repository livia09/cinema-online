<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/cinema-online/css/style.css">
	<title>@yield('title')</title>
</head>
<body>

@if(!empty(Session()) && Session::has('type') && Session::get('type') == 'Admin')

<div class = "container bg-light p-4 my-4 border border-info rounded-sm">
  <div class="d-flex justify-content-center display-4 mb-3">
		@yield('header')
	</div>
</div>

  	<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
  		
  	<h1 class="text-center text-primary">

  		@yield('content_title')

  	</h1>
  	<br>

    <ul class="nav nav-tabs  text-light">
      <li class="nav-item">
        @yield('nav1')
      </li>
      <li class="nav-item">
        @yield('nav2')
      </li>
      <li class="nav-item">
        @yield('nav3')
      </li>
      <li class="nav-item">
        @yield('nav4')
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/cinema-online/visitor/movies">Movies page</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/cinema-online/visitor/movies-theaters">Movies theaters page</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/cinema-online/logout">Logout</a>
      </li>
    </ul>
  	
		@yield('content')

	</div>

@else
<br><br>
<h3 class="text-center">{{$message="Session for this page is logout!"}}</h3>
@endif	
</body>
</html>