<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/cinema-online/css/style.css">
  <title>Welcome</title>
</head>
<body>

	<div class = "container bg-info p-4 my-4 border border-primary rounded-sm">
   <div class="d-flex justify-content-center display-4 mb-3 text-white">
    Cinema online
  </div>
</div>

<div class = "container bg-dark p-5 my-5 border border-primary rounded-sm">

  <h2 class="text-center text-white">
    Welcome on the site! <br><br> Now choose an option and enjoy it!
  </h2>
  
  <br><br><br>
	  <nav class="nav justify-content-center bg-info border border-primary rounded-sm p-1 m-0">
	      <a class="nav-link text-light" href="/cinema-online/register">Register</a>
	      <a class="nav-link text-light" href="/cinema-online/login">Login</a>
	      <a class="nav-link text-light" href="http://localhost/cinema-online/loginAdmin">Admin</a>
	  </nav>
	  <br><br><br><br>
</div>
</body>
</html>