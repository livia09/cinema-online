@extends('layout')

@section('title')
Members
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Add a new member
@endsection
<br>
@section('nav1')
  <a class="nav-link active" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 pl-5">Add a member</h1>
		</div>
		<div class="card-body">
			{!!Form::open(array('action'=>'Admin\MembersController@store'))!!}
			@csrf
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('name','Name:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder'=>'name', 'required'])!!}
					{!!$errors->first('name')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('age','Age:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('age', old('age'), ['class' => 'form-control', 'id' => 'age', 'placeholder'=>'years', 'required'])!!}
					{!!$errors->first('age')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('email','E-mail:')!!}
				</div>
				<div class="col-8">
					{!!Form::email('email', null, ['class' => 'form-control', 'id' => 'email','placeholder'=>'email@domain','required'])!!}
					{!!$errors->first('email')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('password','Password:')!!}
				</div>
				<div class="col-8">
					{!!Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder'=>'password', 'required'])!!}
					{!!$errors->first('password')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('confirm_password','Confirm password:')!!}
				</div>
				<div class="col-8">
					{!!Form::password('confirm_password', ['class' => 'form-control', 'id' => 'confirm_password', 'placeholder'=>'confirm password', 'required'])!!}
					{!!$errors->first('confirm_password')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('type','Type:')!!}
				</div>
				<div class="col-8">
					{{ Form::select('type', ['Visitor' => 'Visitor', 'Registered' => 'Registered', 'Admin' => 'Admin'], 'Visitor', ['class' => 'form-control']) }}
					{!!$errors->first('type')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{{Form::hidden('id')}}
					{!!Form::submit('New member', ['class' => 'btn btn-primary'])!!}
				</div>
			</div>
			@include('errors')
			{!!Form::close()!!}
		</div>
	</div>
</div>

@endsection