@extends('layout')

@section('title')
Members
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Members
@endsection
<br>
@section('nav1')
  <a class="nav-link active" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

@if(count($members)==0)
<div class="d-flex justify-content-center">No members in database!</div>
@else
<table class="container table table-striped table-hover border border-info  text-center py-4 mt-5">
  <tr class="bg bg-primary text-white">
    <th>Name</th>
    <th>Age</th>
    <th>Email</th>
    <th>Password</th>
    <th>Type</th>
    <th>Actions</th>
  </tr>
  @foreach($members as $member)
  <tr>
    <td>{{$member->name}}</td>
    <td>{{$member->age}}</td>
    <td>{{$member->email}}</td>
    <td> 
      @if(strlen($member->password) > 22)
        {{ str_pad(substr($member->password,0,22),25,".") }}
      @else
        {{$member->password}}
      @endif
      </td>
    <td>{{$member->type}}</td>
  </div>
  <td>
    <div class = "row">
      <div class="col-sm">
        {!!Html::link("/admin/members/{$member->id}",'View',['class' => 'btn btn-primary m-1'])!!}
      </div>
      <div class="col-sm">
        {!!Html::link("/admin/members/{$member->id}/edit",'Edit',['class' => 'btn btn-primary m-1'])!!}
      </div>
        <div class="col-sm">
          <form method="post" action="/cinema-online/admin/members/<?php echo $member->id;?>">
            <?php echo method_field('Delete');?>
            <?php echo csrf_field(); ?>
            <button type="submit" class ='btn btn-primary m-1'>Delete</button>
          </form>
        </div>
      </div>
    </td>
  </tr>
  @endforeach
</table>
@endif

<br/>
<div class="container d-flex justify-content-center">
  {!!Html::link("admin/members/create",'New member',['class' => 'btn btn-primary m-1'])!!}</div>
  <br/><br/>
  <div class="d-flex justify-content-center">
    @if(Session::has('message'))
    {{Session::get('message')}}
    @endif
  </div>
</div>
@endsection
