@extends('layout')

@section('title')
Movies theater
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Edit a movie theater
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link active" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-director">
			<h1 class="text-primary p-3 pl-5">Edit movie theater</h1>
		</div>
		<div class="card-body">
			{!!Form::model($movietheater, array('url'=>'/admin/movietheater/'.$movietheater->id))!!}
				{{Form::hidden('id')}} 
				@csrf				
				{{ method_field('PATCH') }}
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('name','Movie theater name:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('theater_name', $movietheater->theater_name, ['class' => 'form-control', 'id' => 'name', 'required'])!!}
					{!!$errors->first('theater_name')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('capacity','Movie theater capacity:')!!}
				</div>
				
				<div class="col-8">
					{!!Form::text('theater_capacity',  $movietheater->theater_capacity, ['class' => 'form-control', 'id' => 'capacity', 'required'])!!}
					{!!$errors->first('theater_capacity')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('hours','Movie theater hour:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('theater_hours',  $movietheater->theater_hours, ['class' => 'form-control', 'id' => 'hours', 'required', 'placeholder' =>'00:00'])!!}
					{!!$errors->first('theater_hours')!!}
				</div>
			</div><br/>

			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{!!Form::submit('Update movie theater', ['class' => 'btn btn-primary'])!!}
				</div>
			</div>
			@include('errors')
			{!!Form::close()!!}
		</div>
	</div>
</div>

@endsection