@php($moviestheater))
@extends('layout')

@section('title')
Movies theater
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Add a movie theater
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link active" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 p-5">Add movie theater</h1>
		</div>
		<div class="card-body">
			{!!Form::open(array('action'=>'Admin\MoviestheaterController@store'))!!}
			@csrf
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('name','Movie theater name:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('theater_name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder'=>'movie name', 'required'])!!}
					{!!$errors->first('theater_name')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('capacity','Movie theater capacity:')!!}
				</div>				
				<div class="col-8">
					{!!Form::text('theater_capacity', null, ['class' => 'form-control', 'id' => 'capacity', 'placeholder'=>'number of places', 'required'])!!}
					{!!$errors->first('theater_capacity')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('hours','Movie theater hour:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('theater_hours', null, ['class' => 'form-control', 'id' => 'hours',  'placeholder' =>'hh:mm:ss', 'required'])!!}
					{!!$errors->first('theater_hours')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">
					{{Form::hidden('id')}}
					{!!Form::submit('Add movie theater', ['class' => 'btn btn-primary'])!!}
				</div>
				@include('errors')
				{!!Form::close()!!}
			</div>
		</div>
	</div>
</div>
@endsection