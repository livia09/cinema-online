@extends('layout')

@section('title')
Movies theater
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Movies theater
@endsection

<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link active" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

@if(count($moviestheater)==0)
<div class="d-flex justify-content-center">No movies theater in database!</div>
@else
<table class="container table table-striped table-hover border border-info text-center py-4 mt-5">
  <tr class="bg bg-primary text-white">
    <th>Theater name</th>
    <th>Theater capacity</th>
    <th>Theater hour</th>
    <th>Actions</th>
  </tr>
  @foreach($moviestheater as $movie)
  <tr>
    <td>{{$movie->theater_name}}</td>
    <td>{{$movie->theater_capacity}}</td>
    <td>{{$movie->theater_hours}}</td>
  </div>
  <td>
    <div class = "row">
      <div class="col-sm">
        {!!Html::link("/admin/movietheater/{$movie->id}",'View',['class' => 'btn btn-primary m-1'])!!}
      </div>
      <div class="col-sm">
        {!!Html::link("/admin/movietheater/{$movie->id}/edit",'Edit',['class' => 'btn btn-primary m-1'])!!}
      </div>
        <div class="col-sm">
          <form method="post" action="/cinema-online/admin/movietheater/<?php echo $movie->id;?>">
            <?php echo method_field('Delete');?>
            <?php echo csrf_field(); ?>
            <button type="submit" class ='btn btn-primary m-1'>Delete</button>
          </form>
        </div>
      </div>
    </td>
  </tr>
  @endforeach
</table>
@endif

<br/>
<div class="container d-flex justify-content-center">
  {!!Html::link("/admin/movietheater/create",'New movie theater',['class' => 'btn btn-primary m-1'])!!}</div>
  <br/><br/>
  <div class="d-flex justify-content-center">
    @if(Session::has('message'))
    {{Session::get('message')}}
    @endif
  </div>
</div>
@endsection