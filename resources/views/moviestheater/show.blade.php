@extends('layout')

@section('title')
Show movie theater
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Details movie theater
@endsection
<br>
@section('nav1')
  <a class="nav-link" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link active" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>
<div class="jumbotron">
	<h1 class="p-2 text-primary">Details for: {{$movie->theater_name}}</h1>
	<br>
	<h3 class="p-2">Id: {{$movie->id}}</h3>
	<h3 class="p-2">Movie theater capacity: {{$movie->theater_capacity}}</h3>
	<h3 class="p-2">Movie theater hours: {{$movie->theater_hours}}</h3>
	<br>
	<span class="p-2">
		{!!Html::link("/admin/movietheater",'Back', ['class' => 'btn btn-primary'])!!}
	</span>
</div>
@endsection