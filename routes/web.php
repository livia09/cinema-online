<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
});

Route::prefix('admin')->group(function () {
	Route::resource('members','Admin\MembersController');
	Route::resource('movies','Admin\MoviesController');
	Route::resource('movietheater','Admin\MoviestheaterController');
	Route::resource('reviews','Admin\ReviewsController');
});

Route::prefix('visitor')->group(function () {
	Route::get('/','MoviesController@index')->name('movies.index');
	Route::get('/movies','MoviesController@viewAllMovies');
	Route::get('/movies-theaters','MoviesController@viewAllMoviesTh');
	Route::get('/movies-theaters/{id}','MoviesController@showTheater');
	Route::get('/movies-theaters/{id}/add-review','MoviesController@createReview');
	Route::post('/movies-theaters','MoviesController@store');
	Route::post('/movies-theaters/booking','MoviesController@bookingstore');
});

Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');

Route::get('/login', 'RegistrationController@login');
Route::post('login', 'RegistrationController@loginin');

Route::get('/loginAdmin', 'RegistrationController@loginAdmin');
Route::post('loginAdmin', 'RegistrationController@logininAdmin');

Route::get('/logout', 'RegistrationController@logout');
