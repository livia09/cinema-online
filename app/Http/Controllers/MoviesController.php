<?php
namespace App\Http\Controllers;

use App\Movie;
use App\Movietheater;
use App\Review;
use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');

    }

    public function viewAllMovies()
    {
        $allmovies = Movie::paginate(1);
    
        foreach ($allmovies as $key => $movie) {
           $allReviews[$movie->id] = $movie->reviews()->getResults();
        }
       
        return view('visitorsPage.viewAllMovies', compact('allmovies','allReviews'));
    }

    public function viewAllMoviesTh(){
        $allTheaters = Movietheater::all();
        
        foreach ($allTheaters as $key => $movie) {
           $allMovies[$movie->id] = $movie->movies()->getResults();    
        }
       
    return view('visitorsPage.viewAllTheaters', compact('allTheaters','allMovies'));
    }

    public function showTheater($id){
        $movietheater = Movietheater::findOrFail($id);
        $allMovies[$movietheater->id] = $movietheater->movies()->getResults();
        foreach ($allMovies[$movietheater->id] as $key => $movie) {
           $allReviews[$movie->id] = $movie->reviews()->getResults();
        }
        
        return view('visitorsPage.showTheater', compact('movietheater', 'allMovies', 'allReviews'));
    }

    public function createReview($id)
    {
        $movie = Movie::findOrFail($id);
        return view('visitorsPage.createReview', compact('movie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Review::create(request()->validate([
            'movie_id' => 'required|numeric|min:1',
            'title' => 'required',
            'member' => 'required|min:5|max:50',
            'rating' => 'required|numeric|min:1',
            'description' => 'required'
        ]));

        Session::flash('message','The review was added!');
        return redirect('/visitor/movies-theaters');
    }

    public function bookingstore(Request $request)
    { 
        //dd($request);
        $validator = Validator::make($request->all(), [
            'theater_id' => 'required',
            'movie_id' => 'required',
            'theater_name' => 'required',
            'movie_title' => 'required',
            'date' => 'required',
            'time' => 'required',
            'price' => 'required',
            'member' => 'required',
            'email' => 'required'
        ]);
        
        $movie = Movie::where(['movieth_id'=>$request->theater_id, 'title'=>$request->movie_title, 'price'=>$request->price, 'release_date'=>$request->date])->select('movieth_id', 'title', 'price', 'release_date')->first();
        $theater = Movietheater::where(['theater_name'=>$request->theater_name, 'theater_hours'=>$request->time])->select('theater_name', 'theater_hours')->first();
        //$review = Review::where(['movie_id'=>$request->movie_id])->select('movie_id')->first();
        //dd($movie, $theater);

        //if($request->movieth_id == $movie->movieth_id && $request->movie_id && $request->theater_name == $theater->theater_name && $request->movie_title == $movie->title && $request->date == $movie->release_date && $request->time == $theater->theater_hours && $request->price == $movie->price && $request->member == Session::get('name') && $request->email == Session::get('email'))
        
            Booking::create([
                'theater_id' => $movie->movieth_id,
                'movie_id' =>  $request->movie_id,
                'theater_name' =>  $theater->theater_name,
                'movie_title' => $movie->title,
                'date' => $movie->release_date,
                'time' => $theater->theater_hours,
                'price' => $movie->price,
                'member' => Session::get('name'),
                'email' => Session::get('email')
            ]); 
        
        // else 
        // {
        //     Session::flash('message','Something goes wrong! Try again!');
        //     return redirect('/visitor/movies-theaters');
        // }

        Session::flash('message','The movie was booked!');
        return redirect('/visitor/movies-theaters');
    }
    
}