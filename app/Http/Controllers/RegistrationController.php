<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Member;
use Session;
use Hash;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.register');
    }

    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'name' => 'required|min:3|max:50',
        'age' => 'required|numeric|min:1',
        'email' => 'required|email|unique:members,email',
        'password'         => 'required|min:10',
        'confirm_password' => 'required|same:password'
    ]);

      if ($validator->fails()) {
        Session::flash('message', "Error! The item didn't has added!");
        return redirect()->back()->withInput();
    }

        // save data
    $member =Member::create([
        'name' => $request->name,
        'age' => $request->age,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'confirm_password' => Hash::make($request->confirm_password),
        'type' => $request->type
    ]);
    
    Session::put(['type' => $request->type, 'age' => $member->age, 'name' => $member->name, 'email' => $member->email]);
    if($request->type == 'Visitor')
        return redirect('/visitor/movies');
    else
        return redirect('/visitor/movies-theaters');
    }

    public function login()
    {
        return view('registration.login');
    }

    public function loginin(Request $request, Member $member)
    {
        $member = Member::where(['email'=>$request->email])->first();
        //dd($member);
        if(isset($member) && !empty($member))        
        {
            if($request->name == $member->name && Hash::check($request->password, $member->password)){
                $requestData = $request->all();
                $requestData['password'] = Hash::make($request->password);
                $requestData['confirm_password'] = Hash::make($request->password);
                $requestData['type'] = $request->type;
                $member->update($requestData);
                Session::put(['type' => $request->type, 'age' => $member->age, 'name' => $member->name, 'email' => $member->email]);
                if($request->type == 'Visitor')
                    return redirect('/visitor/movies');
                else
                    return redirect('/visitor/movies-theaters');
            } else {
                Session::flash('alert','Invalid name or password! Please try again!');
                return redirect('/login');
            }
        }
        else 
        {
            Session::flash('alert', 'Email is not registered! Please register first!');
            return redirect('/login');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/visitor');
    }

    public function loginAdmin()
    {
        return view('registration.loginAdmin');
    }

    public function logininAdmin(Request $request, Member $members)
    {
        $member = Member::where(['email'=>$request->email])->first();

        if(isset($member) && !empty($member))
        {   
            if($member->type != 'Admin'){
                Session::flash('alert', 'Invalid admin!');
                return redirect('/loginAdmin');
            }

            if($request->name == $member->name && Hash::check($request->password, $member->password)){
                Session::put(['type' => 'Admin', 'age' => $member->age, 'name' => $member->name, 'email' => $member->email]);
                return redirect('/admin/members');
            } else {
                Session::flash('alert','Invalid name or password! Please try again!');
                return redirect('/loginAdmin');
            }
        }      

    }
}