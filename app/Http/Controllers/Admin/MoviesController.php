<?php

namespace App\Http\Controllers\Admin;

use App\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Session;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return view('movies.index', compact('movies'));
    }


    public function viewAllMovies()
    {
        $allmovies = Movie::all();
        //dd($allmovies);
        return view('visitorsPage.viewAllMovies', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'movieth_id' => 'required|numeric|min:1',
            'title' => 'required|min:5|max:50',
            'iframe' => 'required|min:5|max:255',
            'release_date' => 'required|date_format:Y-m-d',
            'running_time' => 'required|numeric|min:1',
            'film_genre' => 'required',
            'cast' => 'required',
            'director' => 'required',
            'production' => 'required',         
            'language' => 'required',
            'age_restrict' => 'required|numeric|min:0',
            'price' => 'required|numeric|between:0,9999.99'
        ]);

        if ($validator->fails()) {
            Session::flash('message', "Error! The item didn't has added!");
            return redirect()->back()->withInput();
        }

        // save data
        $movie = Movie::create([
            'movieth_id' => $request->movieth_id,
            'title' => $request->title,
            'iframe' => $request->iframe,
            'release_date' => $request->release_date,
            'running_time' => $request->running_time,
            'film_genre' => $request->film_genre,
            'cast' => $request->cast,
            'director' => $request->director,
            'production' => $request->production,
            'language' => $request->language,
            'age_restrict' => $request->age_restrict,
            'price' => 100*$request->price
        ]);

        Session::flash('message','The item was added!');
        return redirect('/admin/movies');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return view('movies.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //dd($movie);
        return view('movies.edit', compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $request->validate([
            'movieth_id' => 'required|numeric|min:1',    
            'title' => 'required|min:5|max:50',
            'release_date' => 'required|date_format:Y-m-d',
            'running_time' => 'required|numeric|min:1',
            'film_genre' => 'required',
            'cast' => 'required',
            'director' => 'required',
            'production' => 'required',         
            'language' => 'required',
            'age_restrict' => 'required|numeric|min:0',
            'price' => 'required|numeric|between:0,99999.99'
        ]);
  
        $movie->update($request->all());  
        Session::flash('message','The item was updated!');
        return redirect('/admin/movies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie->delete();
  
        Session::flash('message','The item was deleted!');
        return redirect('/admin/movies');
    }
}
