<?php
namespace App\Http\Controllers\Admin;

use App\Movietheater;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;

class MoviestheaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moviestheater = Movietheater::all();
        return view('moviestheater.index', compact('moviestheater'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('moviestheater.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Movietheater::create(request()->validate([
            'theater_name' => 'required|min:5|max:50',
            'theater_capacity' => 'required|numeric|min:1',
            'theater_hours' => 'required|date_format:H:i'
        ]));

        Session::flash('message','The item was added!'); 
        return redirect('/admin/movietheater');        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movietheater  $movietheater
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = Movietheater::findOrFail($id);
        return view('moviestheater.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movietheater  $movietheater
     * @return \Illuminate\Http\Response
     */
    public function edit(Movietheater $movietheater)
    {
        return view('moviestheater.edit', compact('movietheater'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movietheater  $movietheater
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movietheater $movietheater)
    {
        $request->validate([    
            'theater_name' => 'required|min:5|max:50',
            'theater_capacity' => 'required|numeric|min:1',
            'theater_hours' => 'required|date_format:H:i:s'
        ]);  
        $movietheater->update($request->all());  
        Session::flash('message','The item was updated!');
        return redirect('/admin/movietheater');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movietheater  $movietheater
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movietheater $movietheater)
    {
        $movietheater->delete();  
        Session::flash('message','The item was deleted!');
        return redirect('/admin/movietheater');
    }
}