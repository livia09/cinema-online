<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movietheater extends Model
{
   protected $table ='movie_theater';
   protected $fillable = ['theater_name', 'theater_capacity', 'theater_hours'];
   public $timestamps=false;

public function movies()
	{
		return $this->hasMany('\App\Movie', 'movieth_id');
	}
	
}