<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
   protected $fillable = ['movie_id', 'member', 'rating', 'title', 'description'];
   public $timestamps=false;
}
