<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
   protected $table ='movies';
   protected $fillable = ['movieth_id', 'title', 'iframe', 'release_date', 'running_time', 'film_genre', 'cast', 'director', 'production', 'language', 'age_restrict','price'];
   public $timestamps=false;

public function movie_theater()
	{
		return $this->belongsTo(Movietheater::class);
	}

public function reviews()
	{
		return $this->hasMany('\App\Review', 'movie_id');
	}
}