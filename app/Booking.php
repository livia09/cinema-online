<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
   protected $table ='bookings';
   protected $fillable = ['theater_id','movie_id','movie_title','theater_name', 'date','time','price','member','email'];
   public $timestamps=false;

}