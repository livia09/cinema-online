<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
   protected $table ='members';
   protected $fillable = ['name', 'age', 'email', 'password','confirm_password','type'];
   public $timestamps=false;
}
